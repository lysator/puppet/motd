# Sets up the /etc/update-motd.d directory.
# Purges all the initial content. Add new content with the new motd
# define.
class motd::setup {

  # Not techically needed since libpam-modules already includes
  # update-motd functionallity.
  package { 'update-motd':
    ensure => installed,
  }

  file { '/etc/update-motd.d':
    ensure  => directory,
    recurse => true,
    purge   => true,
  }

  # These just make sure we really update (hopefully)

  file_line { 'PAM SSHD remove noupdates':
    ensure            => 'absent',
    path              => '/etc/pam.d/sshd',
    match             => 'pam_motd.so\s*noupdate$',
    match_for_absence => true,
  }

  file_line { 'PAM Login remove noupdates':
    ensure            => 'absent',
    path              => '/etc/pam.d/login',
    match             => 'pam_motd.so\s*noupdate$',
    match_for_absence => true,
  }
}

