define motd::freemem (
  Integer $id = 99,
) {
  motd { $name:
    id      => $id,
    content => epp('motd/freemem.epp', { }),
  }
}
