# Add an entry to the /etc/update-motd.d directory.
define motd (
  String $content,
  Integer $id = 99,
) {

  require ::motd::setup

  $idstr = sprintf('%02d', $id)
  file { "/etc/update-motd.d/${idstr}-${name}":
    # source => $source,
    content => $content,
    mode    => '0755'
  }
}

